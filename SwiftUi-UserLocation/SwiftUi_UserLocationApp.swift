//
//  SwiftUi_UserLocationApp.swift
//  SwiftUi-UserLocation
//
//  Created by Mohamed Saeed on 12/04/2022.
//

import SwiftUI

@main
struct SwiftUi_UserLocationApp: App {
    var body: some Scene {
        WindowGroup {
            MapView()
        }
    }
}
