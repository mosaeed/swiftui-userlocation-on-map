//
//  ContentView.swift
//  SwiftUi-UserLocation
//
//  Created by Mohamed Saeed on 12/04/2022.
//

import MapKit
import SwiftUI

struct MapView: View {
    
    @StateObject private var viewModel = MapViewModel()
    
   
    
    
    var body: some View {
        Map(coordinateRegion: $viewModel.region,showsUserLocation: true)
            .ignoresSafeArea()
            .accentColor(Color(.systemPink))
            .onAppear{
                viewModel.checkIfLocationServicesIsEnabled()
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}


